# MySQL半同步复制集群

## 介绍
本项目旨在搭建一个基于keepalived+GTID半同步复制的MySQL集群，以提供高可用性和数据同步的解决方案。

## 软件架构
本项目使用以下主要软件和组件：
- MySQL 5.7.37
- MySQL Router
- Keepalived
- Ansible

## 系统
Linux centos 7.9

## 使用说明
在完成以上安装和配置后，可以使用MySQL集群来实现高可用性、读写分离和数据同步。根据应用程序需求和业务场景合理配置和管理MySQL集群。




